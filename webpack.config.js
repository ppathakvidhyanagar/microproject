const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env, argv) => {
  const devMode = argv.mode !== "production";

  return {
    entry: {
      app: "./app/app.js"
    },
    output: {
      filename: devMode ? "[name].js" : "[name].bundle.js",
      path: path.resolve(__dirname, "dist"),
      publicPath: "/"
    },
    devtool: "inline-source-map",
    devServer: {
      contentBase: "./dist"
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: "./app/index.html",
        inject: true
      })
    ],
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
              options: { minimize: true }
            }
          ]
        }
      ]
    }
  };
};

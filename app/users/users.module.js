import angular from "angular";
import AngularUIRouter from "angular-ui-router";
import usersController from "./users.controller";
import gitHubFactory from "./../github.factory";

const usersControllerJS = require("./users.controller.js");
const usersHTML = require("./users.html");

const routes = function($stateProvider) {
  $stateProvider.state("users", {
    url: "/users",
    template: usersHTML,
    controller: "usersController as userCtl",
    resolve: {
      loadMyCtrl: [
        "$q",
        "$ocLazyLoad",
        ($q, $ocLazyLoad) => {
          const deferred = $q.defer();
          require.ensure([], () => {
            $ocLazyLoad.inject({ name: usersControllerJS.name });
            deferred.resolve(usersControllerJS);
          });
          return deferred.promise;
        }
      ]
    }
  });
};

routes.$inject = ["$stateProvider"];

export default angular
  .module("users", [AngularUIRouter])
  .controller("usersController", usersController)
  .factory("gitHubFactory", gitHubFactory)
  .config(routes);

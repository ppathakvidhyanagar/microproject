class usersController {
  constructor($state, gitHubFactory) {
    this.$state = $state;
    this._gitHubFactory = gitHubFactory;
    this.count = 0;
    this.users = [];
    this.getUsers();
  }

  getUsers() {
    const ctrl = this;
    ctrl._gitHubFactory.getUsers(ctrl.count).then(response => {
      this.count += response.data.length;
      ctrl.users = ctrl.users.concat(response.data);
    });
  }

  viewProject(login) {
    this.$state.go("projects", { login: login });
  }

  addMore() {
    this.getUsers();
  }
}

usersController.$inject = ["$state", "gitHubFactory"];

export default usersController;

class gitHubFactory {
  constructor($http) {
    this.$http = $http;
    this.root = `https://api.github.com/`;

    //this is for demo else token will be defined in constant file and HTTP interceptor will use to inject it.
    this.token = "b4a86385858a643468590278066a9bebe3f66cc4";
    this.config = {
      headers: {
        Authorization: `token ${this.token}`
      }
    };
  }

  /**
   * 
   * @param {*} index 
   * Get users from github
   */
  getUsers(index) {
    return this.$http.get(`${this.root}users?since=${index}`, this.config);
  }

  /**
   * 
   * @param {*} login 
   * Get projects of specific users
   */
  getProjects(login) {
    return this.$http.get(`${this.root}users/${login}/repos`, this.config);
  }

  /**
   * 
   * @param {*} login 
   * @param {*} repo 
   * 
   * Get readme of specific project by login(username) and rep
   */
  getProjectsDetails(login, repo){
    let configs = {
      headers: {
        "Accept": "application/vnd.github.v3.raw",
        "Authorization": `token ${this.token}`
      }

    }
    return this.$http.get(`${this.root}repos/${login}/${repo}/readme`, configs);
  }
}

gitHubFactory.$inject = ["$http"];

export default gitHubFactory;

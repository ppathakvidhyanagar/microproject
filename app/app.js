import angular from "angular";
import AngularUIRouter from "angular-ui-router";
import AngularUIBootstrap from "angular-ui-bootstrap";
import "oclazyload";

import routes from "./route";
import users from "./users/users.module";
import projects from "./projects/projects.module";
import projectsdetails from "./projectdetails/projectdetails.module";

const app = angular.module("app", [
  AngularUIRouter, // Routing
  AngularUIBootstrap,
  "oc.lazyLoad", // ocLazyLoad
  routes.name,
  users.name,
  projects.name,
  projectsdetails.name
]);

export default app;

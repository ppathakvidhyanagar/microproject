import angular from "angular";

class routes {
  constructor(
    $locationProvider,
    $stateProvider,
    $urlRouterProvider,
    $ocLazyLoadProvider
  ) {
    this.$locationProvider = $locationProvider;
    this.$stateProvider = $stateProvider;
    this.$urlRouterProvider = $urlRouterProvider;
    this.$ocLazyLoadProvider = $ocLazyLoadProvider;
    this.$locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });

    this.$urlRouterProvider.otherwise("/users");
  }
}

routes.$inject = [
  "$locationProvider",
  "$stateProvider",
  "$urlRouterProvider",
  "$ocLazyLoadProvider"
];

export default angular.module("config", []).config(routes);

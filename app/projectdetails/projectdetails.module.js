import angular from "angular";
import AngularUIRouter from "angular-ui-router";
import AngulareSanitize from 'angular-sanitize';

import projectDetailsController from "./projectdetails.controller";
import gitHubFactory from "./../github.factory";

const projectDetailsControllerJS = require("./projectdetails.controller.js");
const projectsHTML = require("./projectdetails.html");

const routes = function($stateProvider) {
  $stateProvider.state("readme", {
    url: "/users/:login/:repo/readme",
    template: projectsHTML,
    controller: "projectDetailsController as projectCtl",
    resolve: {
      loadMyCtrl: [
        "$q",
        "$ocLazyLoad",
        ($q, $ocLazyLoad) => {
          const deferred = $q.defer();
          require.ensure([], () => {
            $ocLazyLoad.inject({ name: projectDetailsControllerJS.name });
            deferred.resolve(projectDetailsControllerJS);
          });
          return deferred.promise;
        }
      ]
    }
  });
};

routes.$inject = ["$stateProvider"];

export default angular
  .module("projectsdetails", [AngularUIRouter, AngulareSanitize])
  .controller("projectDetailsController", projectDetailsController)
  .factory("gitHubFactory", gitHubFactory)
  .config(routes);

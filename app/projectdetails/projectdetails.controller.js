class projectsDetailsController {

    constructor($state, $stateParams, gitHubFactory, $sce) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$sce = $sce;
        this._gitHubFactory = gitHubFactory;
        
        if (this.$stateParams.login === undefined || this.$stateParams.login === "")
            this.users();

        this.login = this.$stateParams.login;

        if (this.$stateParams.repo === undefined || this.$stateParams.repo === "")
            this.projects();

        this.repo = this.$stateParams.repo;

        this.getProjectsDetails();
    }

    getProjectsDetails(){
        const ctrl = this;
        ctrl._gitHubFactory.getProjectsDetails(ctrl.login,ctrl.repo).then((response) => {
            ctrl.readme = ctrl.$sce.trustAsHtml(response.data);;
        });
    }

    users(){
        this.$state.go("users");
    }

    projects(){
        this.$state.go("projects", { login: this.login });
    }
}

projectsDetailsController.$inject = ["$state", "$stateParams", "gitHubFactory", "$sce"];

export default projectsDetailsController;

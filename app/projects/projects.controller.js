class projectsController {

    constructor($state, $stateParams, gitHubFactory) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this._gitHubFactory = gitHubFactory;
        this.projects = [];
        
        if (this.$stateParams.login === undefined || this.$stateParams.login === "")
            this.users();

        this.login = this.$stateParams.login;

        this.getProjects();
    }

    getProjects(){
        const ctrl = this;
        ctrl._gitHubFactory.getProjects(ctrl.login).then((response) => {
            ctrl.projects = response.data;
        });
    }

    users(){
        this.$state.go("users");
    }

    viewProjectDetails(repo){
        this.$state.go("readme",{"login":this.login,"repo":repo});
    }
}

projectsController.$inject = ["$state", "$stateParams", "gitHubFactory"];

export default projectsController;

import angular from "angular";
import AngularUIRouter from "angular-ui-router";
import projectsController from "./projects.controller";
import gitHubFactory from "./../github.factory";

const projectsControllerJS = require("./projects.controller.js");
const projectsHTML = require("./projects.html");

const routes = function($stateProvider) {
  $stateProvider.state("projects", {
    url: "/users/:login",
    template: projectsHTML,
    controller: "projectsController as projectCtl",
    resolve: {
      loadMyCtrl: [
        "$q",
        "$ocLazyLoad",
        ($q, $ocLazyLoad) => {
          const deferred = $q.defer();
          require.ensure([], () => {
            $ocLazyLoad.inject({ name: projectsControllerJS.name });
            deferred.resolve(projectsControllerJS);
          });
          return deferred.promise;
        }
      ]
    }
  });
};

routes.$inject = ["$stateProvider"];

export default angular
  .module("projects", [AngularUIRouter])
  .controller("projectsController", projectsController)
  .factory("gitHubFactory", gitHubFactory)
  .config(routes);

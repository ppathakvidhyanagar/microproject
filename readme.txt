
Clone From Repository
--------------------

Use following commond from repository

git clone https://pranavpathak@bitbucket.org/ppathakvidhyanagar/microproject.git

------------------------

Directory structure

readme.txt          necessary details of the projects.
package.json        To manage the plugins and dependencies.
webpack.config.js   project build configuration based on enviornment.
app/**              contains modules and related controllers, factories and htmls.
dist/**             This folder will create after build and contains build files.

------------------------

Steps to run project.

1. Install dependencies.

    Use following command to install dependencies for Production --> (Suggested)

        npm install --prod

    Use following command to install dependencies for Developement

        npm install

2. Build project

    Use following command to generate Production Build --> (Suggested)

        npm run build-prod  

    Use following command to generate Developement Build

        npm run build-dev

Note: build files will be created under "/dist" folder

3. Run project

    Once you finish 2st step, next step is to run project. To do that use following command.

        npm run start


Note: This project is developed in Ubuntu system and tested in Chrome browser. I suggest you to use Ubuntu system to geneate build and Chrome to test it.
